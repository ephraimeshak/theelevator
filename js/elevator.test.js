/**
 * Created by ephraim on 12/02/17.
 */

describe('elevator', function () {

    beforeEach(module('elevator'));
    var $scope = {};
    var $interval;
    var $controller;
    var $compile;
    var $car;
    var $floors;
    var $panel;

    beforeEach(inject(function(_$interval_, _$controller_){
        $interval = _$interval_;
        $controller = _$controller_;
        var controller = $controller('ElevatorCtrl', { $scope: $scope});

        $car = $scope.car;
        $floors = $scope.floors;
        $panel = $scope.panel;
    }));

    describe('mainFunctionality', function () {
        it('floors construction', function () {
            expect($floors).toEqual([Object({ title: 10, n: 0, open: false, light: null }), Object({ title: 9, n: 1, open: false, light: null }), Object({ title: 8, n: 2, open: false, light: null }), Object({ title: 7, n: 3, open: false, light: null }), Object({ title: 6, n: 4, open: false, light: null }), Object({ title: 5, n: 5, open: false, light: null }), Object({ title: 4, n: 6, open: false, light: null }), Object({ title: 3, n: 7, open: false, light: null }), Object({ title: 2, n: 8, open: false, light: null }), Object({ title: 1, n: 9, open: false, light: null }), Object({ title: 'G', n: 10, open: false, light: null }) ]);
        });

        it('Floor Door should not open if car is moving', function () {

            //It should open as car is not moving
            expect($car.canOpen($car.floor)).toBe(true);

            //Make a call -> start moving the car and then check if floor door can open
            $scope.collectACall($floors[1].n);
            $interval.flush(1000);
            expect($car.canOpen($car.floor)).toBe(false);
        });

        it('car movements', function () {
            expect($car.floor).toBe(3);
            $scope.collectACall($floors[1].n);
            $interval.flush(1000);
            expect($car.floor).toBe(2);
            $interval.flush(1000);
            expect($car.floor).toBe(1);
        });

        it('Calls and car Directions', function () {
            //Calls should be empty
            expect($scope.calls).toEqual([]);
            //Add 2 calls from floor 1,2
            $scope.collectACall($floors[1].n);
            $scope.collectACall($floors[2].n);

            //calls Array should contain the 2 callers
            expect($scope.calls).toEqual([1,2]);

            //run function to get the next direction of the car
            $interval.flush(1000);

            //Check the destination and direction
            expect($car.destination).toBe(1);
            expect($car.dir).toBe(-1);
        });

        /**
         * If the car is occupied but occupant didn't request any destination yet and there are calls appeared.
         * The car should wait until the occupant press a destination.
         * It also will confirm what is the priority of the destination (Always fulfill the occupant request before any call)
         *
         * After the occupant request is fulfilled and the occupant stepped out the elevator can start fulfilling the call requests
         *
         */
        it('Calls and car is occupied', function () {
            var orgCarFloor = $car.floor;
            expect($car.occupied).toBe(false);
            //after stepping in the car should be occupied
            $car.stepIn();
            expect($car.occupied).toBe(true);

            $scope.collectACall($floors[5].n);
            $interval.flush(1000);
            //Car should remain waiting
            expect($car.floor).toBe(orgCarFloor);

            //occupant pressed G floor
            $panel.press(9);
            $interval.flush(2000);
            expect($car.floor).toBe(orgCarFloor + 2);
            $interval.flush(4000);
            //Car has arrived to the desired destination
            expect($car.destination == $car.floor).toBe(true);

            //Occupant can step out Note: in the frontend occupant must open the 2 doors before stepping out
            $car.stepOut();
            $interval.flush(1000);
            //The new destination will be set based on the 1st call
            expect($car.destination).toBe($floors[5].n);
        });


    });

});