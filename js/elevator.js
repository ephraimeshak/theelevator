angular.module("elevator", []).
  controller("ElevatorCtrl", ["$scope", "$interval", function ($scope, $interval) {
    // Object representing the car
    var car = $scope.car = {
      active: function (n) {
        return this.floor == n;
      },
      state: function () {
        var r = this.occupied ? "Occpd " : "Empty ";
        switch (this.dir) {
          case -1: r += "↑↑↑↑"; break;
          case  1: r += "↓↓↓↓"; break;
          case  0: r += this.open ? "OPEN" : "STOP";
        }
        return r;
      },
      canOpen: function (n) {
        // TODO
          //enable only the current floor door if the car is not moving.
          return !!(this.dir == 0 && this.floor == n);
      },
      stepIn: function () { this.occupied = true; this.open = false;},
      stepOut: function () {
          this.occupied = false;
          panel.target = -1;
          this.destination = this.floor;
      },
        //as the car door is manual operation then need to add control to open/close
      carDoorToggle: function () {this.open = !car.open;},

      dir: 0,
      floor: 3,
      open: false,
      occupied: false,
      destination: 3,
      waiting: 0
    };

    // Object representing the control panel in the car
    var panel = $scope.panel = {
      btnClass: function (n) {
        // This can be used to emulate a LED light near or inside the button
        // to give feedback to the user.
          return n == this.target? "target" : null;
      },
      pressAble: function (n) {
          return car.floor==n ? false : true;
      },
      press: function (n) {
          this.target = n;
          this.emergencyStop = false;
      },
      stop: function () {
          this.target = -1;
          this.emergencyStop = true;
      },

        target: -1,
        emergencyStop: false
    };

    // Floors
    var floors = $scope.floors = [];
    for (var i=10; i>0; i--) floors.push({title:i});
    floors.push({title:"G"});

    // Let's have them know their indices. Zero-indexed, from top to bottom.
    // Also let's initialize them.
    floors.forEach(function (floor,n) {
      floor.n = n;
      floor.open = false;
      floor.light = null;
    });


        var calls = $scope.calls = [];
        $scope.collectACall = function(n){
            if(calls.indexOf(n) == -1) {
                calls.push(n);
                floors[n].light = "red";
            }
        };

        $scope.openFloorDoor = function(n){
            if(car.canOpen(n)){
                floors[n].open = true;
                car.waiting = 3;
            }
        };

        /**
         * Function to optimize the next Stop and give car direction based on the car current location
         * input void
         *
         * output @integer carDirection (1, 0, -1)
         */
        function setCarDirection(){
            /**
             * Here can be the logic of prioritization/optimization
             * the direction is coming from the car panel is more important than the call
             * if no direction is coming from the car panel then we can get the best option of the calls
             * for now the best call is what was requested first "First come first serve"
             */
            if((panel.target != -1 || calls.length != 0) && car.destination == car.floor){
                if(panel.target != -1){
                    car.destination = panel.target;

                }if (calls.length != 0 && !car.occupied) {
                    if (car.destination == calls[0]){
                        car.waiting = 3;
                        floors[calls.shift()].light = "";
                    } else {
                        car.destination = calls[0];
                        floors[calls[0]].light = "green";
                    }
                }
            }

            //calculate the car direction
            var carDirection = car.destination - car.floor;
            car.dir = carDirection?carDirection<0?-1:1:0;
            return car.dir;
        }




    $interval(function () {
      // TODO: Move the car if necessary
        //Get the Car direction from the optimization function
        if(car.waiting == 0){
            //Automatic close of the floor door
            floors[car.floor].open = false;
            setCarDirection();
        }else{car.waiting--;}

        //Security if the inner door is open and car is occupied then stop the elevator
        if(car.occupied && car.open || panel.emergencyStop || floors[car.floor].open) car.dir = 0;
        //Move the car depends on the direction
        car.floor = car.floor + car.dir;

    }, 1000);
  }]);
